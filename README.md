# Summary 

This is a sample project that needs some tweaking, it is in essence a House Price Model that needs some parameters:

* CONSTRUCTEDAREA - Total area in square meters
* EFFORTRATE - Effort rate a float value from 0 to 1, indicates how much out of a household salary is devoted to paying their house every month 
* ISDUPLEX - Is this flat a duplex (two floors) - Integer value: 1 true 0 - false
* HASLIFT  - Has this flat an elevator - Integer value: 1 true 0 - false
* ISNEW  - Is this flat a new - Integer value: 1 true 0 - false
* HASAIRCONDITIONING  - Is this flat equipped with air conditioning - Integer value: 1 true 0 - false
* HASTERRACE - Has this flat a balcony - Integer value: 1 true 0 - false
* GARAGETYPEID - Type of parking the dwelling has (3 choices):
  
      ID  SHORTNAME       DESCRIPTION
      0 │ unknown       │ unknown       │
      1 │ depot         │ cochera       │
      2 │ parking space │ plaza parking │

# Development
Install dependencies using [poetry](https://python-poetry.org/docs/#installation):
```bash
poetry install
poetry run python main.py
```
or pip (in a venv):
```bash
pip install -r requirements.txt
python main.py
```

Example:
> curl "http://localhost:6666/sale/valuate?CONSTRUCTEDAREA=100&ISDUPLEX=0&HASLIFT=1&ISNEW=1&HASAIRCONDITIONING=1&HASTERRACE=1&GARAGETYPEID=0&EFFORTRATE=0.3&PROVINCE=1&EFFORTRATE=0.4&api_key=53fb16b3e1acd382046648c74b8cc693"

Docs (6666 is blocked by browsers):
```
PORT=5000 poetry run python main.py
# http://localhost:5000/apidocs
```

# Docker
Build the image
```bash
docker build . -t house-valuation
```
Set the desired env vars or pass `--env-file .env`
```build
docker run --name house-valuation -p 5000:6666 house-valuation 
```
http://localhost:5000/apidocs/

# Tech stack

This application uses flask as web service container and runs on 6666 port, you can test it locally with this http GET request.

There are two endpoints:

* /sale/valuate - returns how much a house is worth in euros
* /rent/valuate - returns how much a house rental is worth in euros

Example:

http://localhost:6666/sale/valuate?CONSTRUCTEDAREA=100&ISDUPLEX=0&HASLIFT=1&ISNEW=1&HASAIRCONDITIONING=1&HASTERRACE=1&GARAGETYPEID=0&EFFORTRATE=0.3&PROVINCE=1&EFFORTRATE=0.4

If it comes in handy for any reason: valuation model was created 0.20.1 de scikit-learn

# Objectives

We need to refactor some topics in this project

* Code is not much organized, can you improve it using modules, classes?
  > Follow the usual Flask structure
* Maybe it is worth make some things more configurable as port, etc ... what would you suggest?
  > Use environment variables with a `.env` for local development
* Logging is overlooked as well, can you improve it?
  > Use either `app.logger` or  `logger = logging.getlogger(__name__)`
* Folders could be re-arranged to make the project more readable, what would you suggest?
  > Follow the usual Flask structure
* Performance could be improved, what tweaks could you implement to make it more productive?
  > Load model and csv only once
* We need control the lib versions of the application, how you would manage the env and library dependencies?
  > Requirements.txt / Poetry / Pipenv / etc
* Parameters are passed with no control, can we skill this horrible 500 error when any param is passed wrong?
  * We should consider setting some values by default when they do not come:
    * Default province code should be 28
    * Default effort rate should be 0.34
    * we should control the ranges if this question is applicable to any param
  > Use `marshmallow` or `webargs` to validate the query
* If we need to deploy this api in a large infrastructure to handle a great deal of concurrent requests (on premise). What would you suggest to do?
  > Dockerize and deploy with docker swarm / kubernetes
* We need to track the access to the API for such purpose we need an additional param called **api_key** and control the access to these two keys
  * 53fb16b3e1acd382046648c74b8cc693
  * 3712163876543f00fc342d3e35daa9f8
  * 2d02e669731cbade6a64b58d602cf2a4
  * 925544d7f90cd3663531546f080bbed8

Some other topics regarding documentation:

* Documentation is poor, what best practices would you apply here?
  > Type hinting / follow a docstring style (reST/ Google / Numpy)
* Undocumented API are no so nice, how could we have an Open API specs to this simple api? 
  > Use a library to extract OpenAPI-Spec from code (flasgger) or migrate to [FastAPI](https://fastapi.tiangolo.com/#interactive-api-docs)
  
