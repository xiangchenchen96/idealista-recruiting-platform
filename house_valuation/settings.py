"""Application configuration.
Most configuration is set via environment variables.
For local development, use a .env file to set
environment variables.
"""
from environs import Env

env = Env()
env.read_env()

ENV = env.str("FLASK_ENV", default="production")
DEBUG = ENV == "development"

MODEL_PATH = env.str("MODEL_PATH", "valuation-model.pickle")
RENTAL_YIELD_CSV = env.str(
    "RENTAL_YIELD_CSV",
    "https://github.com/davidreyblanco/ml-training/raw/master/data/investment/synthetic-rental-yield.csv.gz",
)

HOST = env.str("HOST", "localhost")
PORT = env.int("PORT", 6666)
API_KEYS = env.list(
    "API_KEYS",
    [
        "53fb16b3e1acd382046648c74b8cc693",
        "3712163876543f00fc342d3e35daa9f8",
        "2d02e669731cbade6a64b58d602cf2a4",
        "925544d7f90cd3663531546f080bbed8",
    ],  # Assuming these keys are not for production
)
