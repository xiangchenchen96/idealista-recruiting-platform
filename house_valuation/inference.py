from typing import Tuple

import pandas as pd
from house_valuation.utils import get_model

# our features will be parameters
# IS_ or HAS_ prefixed (or clauses) are read as
# 0 = No 1 = Yes
FEATURES = [
    "CONSTRUCTEDAREA",
    "EFFORTRATE",
    "ISDUPLEX",
    "HASLIFT",
    "ISNEW",
    "HASAIRCONDITIONING",
    "HASTERRACE",
    "GARAGETYPEID",
]


def predict_sale_price(data: dict) -> Tuple[float, float]:
    """Predict the sqm_sale_price and total_sale_price of the given house."""
    model = get_model()
    input_df = pd.DataFrame.from_dict(data, orient="index").T

    # we filter the model feature set and evaluate model
    # the model returns prices in €/m²
    model_input = pd.DataFrame(input_df[FEATURES])
    model_output = model.predict(model_input)
    return model_output[0], model_output[0] * data["CONSTRUCTEDAREA"]
