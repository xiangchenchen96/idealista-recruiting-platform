from webargs import fields, validate
from werkzeug.exceptions import Forbidden
from house_valuation.settings import API_KEYS

_boolean_validator = validate.OneOf([0, 1])

query_args = {
    "EFFORTRATE": fields.Float(missing=0.34),
    "PROVINCE": fields.Int(missing=28),
    "CONSTRUCTEDAREA": fields.Float(required=True),
    "ISDUPLEX": fields.Int(validate=_boolean_validator, required=True),
    "HASLIFT": fields.Int(validate=_boolean_validator, required=True),
    "ISNEW": fields.Int(validate=_boolean_validator, required=True),
    "HASAIRCONDITIONING": fields.Int(validate=_boolean_validator, required=True),
    "HASTERRACE": fields.Int(validate=_boolean_validator, required=True),
    "GARAGETYPEID": fields.Int(required=True),
    "api_key": fields.Str(),
}


def validate_api_key(data: dict):
    api_key = data.pop("api_key", None)
    if api_key not in API_KEYS:
        raise Forbidden
    return api_key
