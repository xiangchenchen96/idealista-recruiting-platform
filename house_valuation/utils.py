import pickle
from functools import lru_cache

import pandas as pd
from flask import current_app

from house_valuation.settings import RENTAL_YIELD_CSV, MODEL_PATH


def calculate_rent_price_from_yield(sale_price: float, yield_: float) -> float:
    """Create Rent Price from gross yield and sale price"""
    r = sale_price * yield_ / 12
    return r


def get_gross_yield(province: str) -> float:
    """Get the gross_yield of the given province"""
    df_yield = get_rental_yield()
    df_province_yield = df_yield[(df_yield.PROVINCE == province)]
    gross_yield = float(df_province_yield["YIELD"])
    return gross_yield


@lru_cache(maxsize=1)
def get_rental_yield(path=RENTAL_YIELD_CSV) -> pd.DataFrame:
    """
    Get the rental yield for a house for each province
    thus we can calculate the rental price following this formula::
        yield = 12 * rent-price / sale-price
    """
    current_app.logger.info(f"Reading CSV: {path}")
    return pd.read_csv(path, sep=";")


@lru_cache(maxsize=1)
def get_model(path=MODEL_PATH):
    """Load a model pickle."""
    current_app.logger.info(f"Reading Model Pickle: {path}")
    return pickle.load(open(path, "rb"))
