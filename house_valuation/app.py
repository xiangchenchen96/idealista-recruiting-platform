"""
Flask microservice that predicts the rent and sale price of a house.

@author: Xiang Chen

Sample call:
http://localhost:6666/rent/valuate?CONSTRUCTEDAREA=100&ISDUPLEX=0&HASLIFT=1&ISNEW=1&HASAIRCONDITIONING=1&HASTERRACE=1&GARAGETYPEID=0&EFFORTRATE=0.3&PROVINCE=1&EFFORTRATE=0.4
"""

import sklearn
from flask import Flask, jsonify
from flasgger import Swagger

from house_valuation.routes.sale import sale_bp
from house_valuation.routes.rent import rent_bp


def create_app():
    app = Flask(__name__)
    swagger = Swagger(app)
    app.register_blueprint(sale_bp, url_prefix="/sale")
    app.register_blueprint(rent_bp, url_prefix="/rent")
    _add_error_handler(app)

    app.logger.info(f"The scikit-learn version is {sklearn.__version__}.")
    return app


def _add_error_handler(app):
    """Return validation errors as JSON"""

    @app.errorhandler(422)
    @app.errorhandler(400)
    def handle_error(err):
        headers = err.data.get("headers", None)
        messages = err.data.get("messages", ["Invalid request."])
        if headers:
            return jsonify({"errors": messages}), err.code, headers
        else:
            return jsonify({"errors": messages}), err.code
