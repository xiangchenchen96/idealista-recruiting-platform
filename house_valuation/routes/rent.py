from flask import Blueprint, jsonify
from webargs.flaskparser import use_args

from house_valuation.utils import calculate_rent_price_from_yield, get_gross_yield
from house_valuation.inference import predict_sale_price
from house_valuation.query_validation import query_args, validate_api_key

rent_bp = Blueprint("rent_bp", __name__)


@rent_bp.get("/valuate")
@use_args(query_args, location="query")
def valuate_rent(data):
    """TODO: Valuate the rent of a given house.
    ---
    parameters:
      - name: EFFORTRATE
        in: query
        type: float
        default: 0.34
    definitions:
      RentValuation:
        type: object
        properties:
          rent_price:
            type: integer
          rent_price_sqm:
            type: integer
          parameters:
            type: object
    responses:
      200:
        description: Rent prices
        schema:
          $ref: '#/definitions/RentValuation'
    """
    validate_api_key(data)

    sqm_sale_price, total_sale_price = predict_sale_price(data)
    sqm_sale_price = round(sqm_sale_price)
    total_sale_price = round(total_sale_price)

    # get the yield for the calling province
    gross_yield = get_gross_yield(data["PROVINCE"])

    # Finally we work out the rent price
    model_sqm_rent_price = calculate_rent_price_from_yield(sqm_sale_price, gross_yield)
    model_total_rent_price = round(total_sale_price * gross_yield / 12)

    # we build the payload
    out = {
        "rent_price": model_total_rent_price,
        "rent_price_sqm": model_sqm_rent_price,
        "parameters": data,
    }

    status_code = 200
    return jsonify(out), status_code
