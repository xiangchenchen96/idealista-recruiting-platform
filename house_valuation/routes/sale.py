from flask import Blueprint, jsonify
from webargs.flaskparser import use_args

from house_valuation.inference import predict_sale_price
from house_valuation.query_validation import query_args, validate_api_key

sale_bp = Blueprint("sale_bp", __name__)


@sale_bp.get("/valuate")
@use_args(query_args, location="query")
def valuate_sale(data):
    """TODO: Valuate the sale price of a given house.
    ---
    parameters:
      - name: EFFORTRATE
        in: query
        type: float
        default: 0.34
    definitions:
      SaleValuation:
        type: object
        properties:
          sale_price:
            type: integer
          sale_price_sqm:
            type: integer
          parameters:
            type: object
    responses:
      200:
        description: Sale prices
        schema:
          $ref: '#/definitions/SaleValuation'
    """
    validate_api_key(data)

    sqm_sale_price, total_sale_price = predict_sale_price(data)
    out = {
        "sale_price": round(total_sale_price),
        "sale_price_sqm": round(sqm_sale_price),
        "parameters": data,
    }

    status_code = 200
    return jsonify(out), status_code
