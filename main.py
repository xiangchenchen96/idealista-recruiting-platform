from house_valuation.app import create_app
from house_valuation.settings import HOST, PORT

if __name__ == "__main__":
    app = create_app()
    app.run(host=HOST, port=PORT, threaded=True)
