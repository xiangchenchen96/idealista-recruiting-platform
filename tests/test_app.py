query_base = "CONSTRUCTEDAREA=100&ISDUPLEX=0&HASLIFT=1&ISNEW=1&HASAIRCONDITIONING=1&HASTERRACE=1&GARAGETYPEID=0&EFFORTRATE=0.3&PROVINCE=1&EFFORTRATE=0.4"
api_key = "&api_key=53fb16b3e1acd382046648c74b8cc693"
sample_query = query_base + api_key


def test_forbidden(client):
    res = client.get(f"/sale/valuate?{query_base}")
    assert res.status_code == 403


def test_execute_valuation1(client):
    res = client.get(f"/sale/valuate?{sample_query}")
    assert res.json == {
        "parameters": {
            "CONSTRUCTEDAREA": 100.0,
            "EFFORTRATE": 0.3,
            "GARAGETYPEID": 0.0,
            "HASAIRCONDITIONING": 1.0,
            "HASLIFT": 1.0,
            "HASTERRACE": 1.0,
            "ISDUPLEX": 0.0,
            "ISNEW": 1.0,
            "PROVINCE": 1.0,
        },
        "sale_price": 301456,
        "sale_price_sqm": 3015,
    }


def test_execute_valuation2(client):
    res = client.get(f"/rent/valuate?{sample_query}")
    assert res.json == {
        "parameters": {
            "CONSTRUCTEDAREA": 100.0,
            "EFFORTRATE": 0.3,
            "GARAGETYPEID": 0.0,
            "HASAIRCONDITIONING": 1.0,
            "HASLIFT": 1.0,
            "HASTERRACE": 1.0,
            "ISDUPLEX": 0.0,
            "ISNEW": 1.0,
            "PROVINCE": 1.0,
        },
        "rent_price": 998,
        "rent_price_sqm": 9.983248483493703,
    }
