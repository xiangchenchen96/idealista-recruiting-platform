FROM python:3.7-slim

COPY . /app
WORKDIR app

RUN pip install --upgrade pip \
    && pip install -U poetry \
    && poetry config virtualenvs.create false \
    && poetry install --no-interaction --no-ansi --no-dev

CMD ["gunicorn", "--bind", "0.0.0.0:6666", "house_valuation.app:create_app()"]
