import pytest
from house_valuation.app import create_app


@pytest.fixture
def app():
    app = create_app()
    yield app


@pytest.fixture
def client(app):
    app.config["TESTING"] = True

    with app.test_client() as client:
        yield client
